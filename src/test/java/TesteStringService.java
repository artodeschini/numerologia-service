import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.todeschini.service.StringService;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class TesteStringService {

    @Inject
    StringService service;

    @Test
    /**
     * @param Number
     *
     * Retorna a soma de todos os numeros um a um
     * exemplo 2018 -- > 2 + 0 + 1 + 8 = 11 ...
     *
     * @return Numeber
     */
    public void testSomaTodosDigitos() {
        int calculo = service.somaTodosDigitos(2018);

        assertEquals(calculo, 11);
    }

    @Test
    public void testValorLetra() {
        String letrasValue1 = "AaJjSs";
        for (int i = 0; i < letrasValue1.length(); i++) {
            assertEquals(service.letraValue(letrasValue1.charAt(i)), 1);
        }

        String letrasValue2 = "BbKkTt";
        for (int i = 0; i < letrasValue2.length(); i++) {
            assertEquals(service.letraValue(letrasValue1.charAt(i)), 2);
        }
    }
}
