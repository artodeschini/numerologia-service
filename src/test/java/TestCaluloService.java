import io.quarkus.test.junit.QuarkusTest;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.todeschini.service.CalculoService;
import org.todeschini.service.StringService;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;


@QuarkusTest
public class TestCaluloService {

    @Inject
    CalculoService service;

    @Test
    public void testeCalculoIdade() {
        String instantExpected = "2020-09-12T10:15:30Z";
        Clock clock = Clock.fixed(Instant.parse(instantExpected), ZoneId.of("UTC"));

        Instant instant = Instant.now(clock);

        assertEquals(instant.toString(), instantExpected);

        int[] expected = { 43, 5,  20 };

        int[] atual = service.calculoIdade(LocalDate.of(1977, 3, 23));

        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], atual[i]);
        }
    }
}
