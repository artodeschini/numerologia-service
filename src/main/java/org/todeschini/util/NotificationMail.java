package org.todeschini.util;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.vertx.core.json.Json;
import org.apache.commons.io.IOUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class NotificationMail {

    @Inject
    Mailer mailer;

    public void sendEmail(String to, String cc, String body) {
        mailer.send(Mail.withText(to,cc, body));
    }

}
