package org.todeschini.util;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

@ApplicationScoped
public class DateUtils {

    public long inMilliSeconds(LocalDate date) {
        LocalDateTime dateTime = date.atTime(LocalTime.MIN);
        return dateTime.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
    }
}
