package org.todeschini.resource;

import org.todeschini.service.CalculoService;
import org.todeschini.service.CalendarioChinesService;
import org.todeschini.vo.CalculoVO;
import org.todeschini.vo.entrada.CalculoKuaEntradaVO;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.logging.Logger;

@Path("/calculo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NumerologiaResourse {

    private static final Logger LOGGER = Logger.getLogger(NumerologiaResourse.class.getName());

    @Inject
    CalculoService service;

    @Inject
    CalendarioChinesService chinesService;

    @POST
    public Response calculo(CalculoVO calculoVO) {
        //service.calculoCasa(calculoVO);
        return Response.ok(service.calculoNumerologico(calculoVO)).build();
    }

    @GET
    @Path("/chines/{data}")
    public Response calendarioChines(@PathParam("data") String data) {
        //return Response.ok(chinesService.getCalendarioChines(data)).build();
        return Response.ok().build();
    }


    @GET
    @Path("/casa/{numero}")
    @Produces(MediaType.TEXT_HTML)
    public Response calculoCasa(@PathParam("numero") String numero) {
        int caculo = service.calculoCasa(numero);

        String msg = "<b>O número da sua casa é</b>: {0} <br><b>Segundo a numerologia é ... </b><br>{1}";

        return Response.ok(MessageFormat.format(msg, new Object[]{numero, caculo})).build();
    }

    @POST
    @Path("/kua")
    public Response calculoKua(CalculoKuaEntradaVO entrada) {
        return Response.ok(service.calculoKua(entrada)).build();
    }

}
