package org.todeschini.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.todeschini.entity.Usuario;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class UsuarioRepository implements PanacheRepository<Usuario> {

}
