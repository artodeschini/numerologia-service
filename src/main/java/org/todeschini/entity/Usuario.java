package org.todeschini.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Usuario extends PanacheEntity {

    @Column(name = "")
    private String nome;

    @EqualsAndHashCode.Include
    @Column
    private String email;

    @EqualsAndHashCode.Include
    @Column
    private String login;
    @Column
    private String senha;
    @Column
    private int quantidade;
}
