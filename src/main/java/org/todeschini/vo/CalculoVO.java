package org.todeschini.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CalculoVO implements Serializable {

    private String nome;
    private int sexo;
    private String dataNascimento;
    private int numeroCasa;

    @Builder
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CalendarioChinesVO {

        private LocalDate inicio;
        private LocalDate fim;

        private String animal;
        private String elemento;

        private int sinal;
    }
}
