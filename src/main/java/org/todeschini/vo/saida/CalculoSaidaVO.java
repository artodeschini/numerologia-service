package org.todeschini.vo.saida;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.todeschini.vo.CalculoVO;
import org.todeschini.vo.CoresVO;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CalculoSaidaVO implements Serializable {

//            nomeOriginal,
//            idade,
//            signos[ numeroSigno ],
//            planetas[ numeroSigno ],
//            elemento,
//            numero( ambicao ),
//    numero( expres ),
//    numero( personal ),
//    numero( destino ),
//    peloNas[ dn.getDate() ],
//    amb[ numero( ambicao ) ],
//    pers[ numero( personal ) ],
//    exp[ numero( expres ) ],
//    destinos[ numero( destino ) ],
//    numKua,
//    tableKua,
//    textAusencias,
//    signoSolarPedras[ numeroSigno ],
//
//    calendariChines[2],
//    calendariChines[3],
//    calendariChines[4]

    private String nome;
    private String idade;
    private LocalDate dataNascimento;

    private int signo;
    private String signoTexto;

    private String planeta;
    private String planetaTexto;

    private String elemento;
    private String elementoTexto;

    private int ambicao;
    private String ambicaoTexto;

    private int expres;
    private String expresTexto;

    private int personal;
    private String personalTexto;

    private int destino;
    private String destinoTexto;

    private List<CoresVO> ausencias;

    private CalculoVO.CalendarioChinesVO calendarioChines;

    private int numKua;
    private List<String> coordenadasKua;

}
