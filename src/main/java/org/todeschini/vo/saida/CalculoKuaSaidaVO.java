package org.todeschini.vo.saida;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculoKuaSaidaVO {

    private String nome;
    private String sexo;
    private int numero;
    private List<String> coordenadas;
}
