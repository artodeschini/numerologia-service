package org.todeschini.vo.entrada;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalculoKuaEntradaVO {

    private String nome;
    private String dataNascimento;
    private int sexo;

}
