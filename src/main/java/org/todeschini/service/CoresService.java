package org.todeschini.service;

import io.quarkus.runtime.StartupEvent;
import org.todeschini.vo.CoresVO;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
public class CoresService {

    private CoresVO[] cores = new CoresVO[9];

    public CoresService() {
        onLoad();
    }

    public void onStart(@Observes StartupEvent ev) {
        onLoad();
    }

    public void onLoad() {
        cores[0] = CoresVO.builder().regex("(A|J|S)").cor("vermelho").descricao("excitante e energético").build();
        cores[1] = CoresVO.builder().regex("(B|K|T)").cor("laranja").descricao("aspiração, sociabilidad").build();
        cores[2] = CoresVO.builder().regex("(C|L|U)").cor("amarelo").descricao("positividade, otimismo").build();
        cores[3] = CoresVO.builder().regex("(D|M|V)").cor("verde").descricao("paz, tranqüilidade").build();
        cores[4] = CoresVO.builder().regex("(E|N|W)").cor("azul claro").descricao("otimismo, segurança").build();
        cores[5] = CoresVO.builder().regex("(F|O|X)").cor("anil").descricao("segurança").build();
        cores[6] = CoresVO.builder().regex("(G|P|Y)").cor("lilás").descricao("espiritualidade, ideais elevados").build();
        cores[7] = CoresVO.builder().regex("(H|Q|Z)").cor("rosa").descricao("amorosidade, simpatia").build();
        cores[8] = CoresVO.builder().regex("(I|R)").cor("branco").descricao("pureza, brilho").build();
    }

    public List<CoresVO> checkCoresAusentes(String nome) {
        List<CoresVO> ausencias = new ArrayList<>();

        String regex;
        Pattern pattern;
        Matcher matcher;

        int x = 0;

        for (int i = 0; i < cores.length; i++) {
            //regex =  new RegExp( cores[i][0] , "i");
            regex = cores[i].getRegex();
            pattern = Pattern.compile(regex);
            matcher = pattern.matcher(nome);

            if ( !matcher.find() ) {
                ausencias.add(cores[i]);
            }
        }

        return ausencias;

    }
}
