package org.todeschini.service;

import org.todeschini.vo.*;
import org.todeschini.vo.entrada.CalculoKuaEntradaVO;
import org.todeschini.vo.saida.CalculoKuaSaidaVO;
import org.todeschini.vo.saida.CalculoSaidaVO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class CalculoService {

    @Inject
    SignoService signoService;

    @Inject
    StringService stringService;

    @Inject
    CalendarioChinesService chinesService;

    @Inject
    CoresService coresService;

    @Inject
    DadosSaida dados;

    public int calculoCasa(String calculo) {
        calculo = calculo.trim();
        int soma = 0;
        char c;
        for (int i = 0; i < calculo.length(); i++) {
            c = calculo.charAt(i);

            if (Character.isDigit(c)) {
                soma += Integer.parseInt(String.valueOf(c));
            } else {
                soma += stringService.letraValue(c);
            }
        }
        return stringService.somaTotal(soma);
    }

    /**
     * @return Array com dados de quantos anos, meses e dias a pessoa tem segundo a sua data de nascimento
     * @parametro Date
     */
    public int[] calculoIdade(LocalDate dataNascimento) {
        LocalDate today = LocalDate.now();
        int anos, meses, dias;

        //ano atual - ano de nascimento
        anos = today.getYear() - dataNascimento.getYear();

        //meses atual - mes de nascimento
        meses = today.getMonthValue() - dataNascimento.getMonthValue();

        //dias = dia atual - dia de nascimento
        dias = today.getDayOfMonth() - dataNascimento.getDayOfMonth();

        if (meses < 0) {
            anos--;
            meses = 12 + meses;

            if (dias < 0) {
                dias = 31 + dias;
                meses--;
            }

        } else if (meses == 0) {

            if (dias < 0) {
                anos--;
                meses = 11;
                dias = 31 + dias;
            }

        } else {

            if (dias < 0) {
                dias = 31 + dias;
                meses--;
            }
        }
        return new int[]{anos, meses, dias};
    }

    /**
     * @return Array [ todasLetras, vogais , consoantes ]
     * @parametro String
     * <p>
     * Calcula a somatória dos valores das letras do nome da pessoa
     * 0            1        2
     */
    public int[] calculoValorNome(String nome) {

        int somaLetras = 0;
        int somaVogais = 0;
        int somaConsoantes = 0;

        String textoCalculo = stringService.removeAcento(nome);

        char c;
        int valor;

        for (int i = 0; i < textoCalculo.length(); i++) {
            c = textoCalculo.charAt(i);
            valor = stringService.letraValue(c);

            somaLetras += valor;

            // verifica se e vogal
            if (String.valueOf(c).matches("[AaEeIiOoUu]")) {
                somaVogais += valor;
            } else {
                somaConsoantes += valor;
            }
        }

        return new int[]{somaLetras, somaVogais, somaConsoantes};
    }

    /**
     * parametro Number
     * <p>
     * Rescebe um numero segundo o signo e retorno o elemento ['Ar', 'Fogo', 'Ar', 'Terra']
     *
     * @retrun elemento
     */
    public String calculoElemento(int signo) {
        String elemento;

        switch (signo) {
            case 3:
            case 7:
            case 11:
                elemento = "Ar";
                break;

            case 4:
            case 8:
            case 12:
                elemento = "Fogo";
                break;

            case 2:
            case 6:
            case 10:
                elemento = "Ar"; //TODO verificar essa merda aqui
                break;
            default:
                elemento = "Terra";
                break;
        }

        return elemento;

//        if (signo == 3 || signo == 7 || signo == 11) {
//            return "Ar";
//
//        } else if (signo == 4 || signo == 8 || signo == 12) {
//            return "Fogo";
//
//        } else if (signo == 2 || signo == 6 || signo == 10) {
//            return "Ar";
//
//        } else {
//            return "Terra";
//        }
    }

    public int numero(int num) {
        int end = 0;
        String nums;
        int f;

        do {
            if (num < 10 || num == 11 || num == 22) {
                end = num;
                break;
            }

            nums = "";
            nums = String.valueOf(num);
            num = 0;

            for (f = 0; f < nums.trim().length(); f++) {
                //num = num + new Number( mid(nums,f,1) );
                num = num + Integer.valueOf(stringService.mid(nums, f, 1));
            }

        } while (true);

        return end;
    }

    /**
     * recebe uma Date com os dados da data de nascimento da pessoa
     * <p>
     * Realiza a decomposição dos dados somando os um a um d + d + M + M + y + y + y + y
     * <p>
     * retorna a soma Number
     */
    public int somaData(LocalDate dn) {
        //soma dos valores do dia
        int dia = dn.getDayOfMonth();
        int mes = dn.getMonth().getValue();
        int ano = dn.getYear();

        StringBuilder dataString = new StringBuilder()
                .append(dia)
                .append(mes)
                .append(ano);

        return numero(Integer.parseInt(dataString.toString()));
    }

    public CalculoKuaSaidaVO calculoKua(CalculoKuaEntradaVO kua) {
        //TODO verificar o formato do front
        LocalDate dn = LocalDate.parse(kua.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        int anoCalculo = dn.getYear();

        if (dn.getMonthValue() == 1 || dn.getMonthValue() == 2) {
            anoCalculo = chinesService.checkCalendarioChines(dn);
        }

        int numero = stringService.somaKua(anoCalculo, kua.getSexo());

        CalculoKuaSaidaVO saida = CalculoKuaSaidaVO.builder()
                .nome(kua.getNome())
                .numero(numero)
                .sexo(kua.getSexo() == 1 ? "Masculino": "Feminino")
                .coordenadas(Arrays.asList(dados.coordenadasKua.get(numero))).build();

        return saida;
    }

    public CalculoSaidaVO calculoNumerologico(CalculoVO calculo) {
        //o calculo se da com o nome em letras maiusculas e sem acentos
        String nome = stringService.removeAcento(calculo.getNome().toUpperCase());

        int ambicao = 0, personal = 0, expres = 0, destino = 0;

        //TODO verificar o formato do front
        LocalDate dn = LocalDate.parse(calculo.getDataNascimento(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        int numeroSigno = signoService.getSigno(dn);

        int[] nomeCalculado = this.calculoValorNome(nome); // [ todasLetras, vogais , consoantes ]

        expres = nomeCalculado[0];   // soma de todas as letras
        ambicao = nomeCalculado[1];  // soma das vogais
        personal = nomeCalculado[2]; // soma das consoantes

        String elemento = calculoElemento(numeroSigno);

        int[] idadeCalculada = calculoIdade(dn);

        String idade = String.valueOf(idadeCalculada[0])
                .concat(" anos, ")
                .concat(String.valueOf(idadeCalculada[1]))
                .concat(" meses e ")
                .concat(String.valueOf(idadeCalculada[2]))
                .concat(" dias.");

        destino = somaData(dn);

        int anoCalculo = dn.getYear();

        if (dn.getMonthValue() == 1 || dn.getMonthValue() == 2) {
            anoCalculo = chinesService.checkCalendarioChines(dn);
        }

        //checar validacao do ano se há necessidade de estar dentro do range de dadas
        CalculoVO.CalendarioChinesVO calendarioChines = chinesService.getCalendarioChines((anoCalculo + 1));

        int numKua = stringService.somaKua(anoCalculo, calculo.getSexo());

//        var tableKua = montaTabelaKua( numKua, coordenadasKua[ numKua ] );

        List<CoresVO> ausencias = coresService.checkCoresAusentes(nome);

//        var textAusencias = showAusencias( ausencias );

        //prepara os dados de saida
        CalculoSaidaVO saida = CalculoSaidaVO.builder()
                .nome(calculo.getNome()) // nome original
                .idade(idade) // idade detalhada de ano meses e dias
                .dataNascimento(dn)
                .ambicao(ambicao)
                .ambicaoTexto(dados.amb.get(ambicao))
                .destino(destino)
                .destinoTexto(dados.destinos.get(destino))
                .personal(personal)
                .personalTexto(dados.pers.get(personal))
                .expres(expres)
                .expresTexto(dados.exp.get(expres))
                .elemento(elemento)
//                .elementoTexto(dados.)
                .ausencias(ausencias)
                .numKua(numKua)
                .coordenadasKua(Arrays.asList(dados.coordenadasKua.get(numKua)))
                .calendarioChines(calendarioChines)
                .build();

        return saida;
    }
}
