package org.todeschini.service;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class StringService {

    /**
     * IN: str - the string we are LEFTing
     * start - our string's starting position (0 based!!)
     * len - how many characters from start we want to get
     * <p>
     * RETVAL: The substring from start to start+len
     */
    public String mid(String str, int start, int len) {
        // Make sure start and len are within proper bounds
        if (start < 0 || len < 0) {
            return "";
        }

        int iEnd = 0;
        int iLen = str.length();

        if (start + len > iLen) {
            iEnd = iLen;
        } else {
            iEnd = start + len;
        }

        return str.substring(start, iEnd);
    }

    /**
     * Retorna a soma de todos os numeros enquanto eles tiverem mais de um digito
     * exemplo 2018 -- > 2 + 0 + 1 + 8 = 11
     * (11 tem mais de dois digito entra no laço novamente)
     * 11 --> 1 + 1 = 2
     * O valor retornado seria 2
     *
     * @return Numeber
     */
    public int somaTotal(int numero) {
        int soma = 0;
        String valor = String.valueOf(numero);

        while (valor.length() > 1) {
            soma = somaTodosDigitos(valor);

            valor = String.valueOf(soma);
            soma = 0;
        }

        return Integer.valueOf(valor).intValue();
    }

    public int somaTodosDigitos(int valor) {
        return somaTodosDigitos(String.valueOf(valor));
    }

    public String removeAcento(String text) {
        return text.replaceAll("[ãâàáä]", "a")
                .replaceAll("[êèéë]", "e")
                .replaceAll("[îìíï]", "i")
                .replaceAll("[õôòóö]", "o")
                .replaceAll("[ûúùü]", "u")
                .replaceAll("[ÃÂÀÁÄ]", "A")
                .replaceAll("[ÊÈÉË]", "E")
                .replaceAll("[ÎÌÍÏ]", "I")
                .replaceAll("[ÕÔÒÓÖ]", "O")
                .replaceAll("[ÛÙÚÜ]", "U")
                .replace('ç', 'c')
                .replace('Ç', 'C')
                .replace('ñ', 'n')
                .replace('Ñ', 'N')
                .replaceAll("!", "")
                .replaceAll("\\[\\´\\`\\?!\\@\\#\\$\\%\\¨\\*", " ")
                .replaceAll("\\(\\)\\=\\{\\}\\[\\]\\~\\^\\]", " ")
                .replaceAll("[\\.\\;\\-\\_\\+\\'\\ª\\º\\:\\;\\/]", " ");
    }

    /**
     * Retorna a soma de todos os numeros um a um
     * exemplo 2018 -- > 2 + 0 + 1 + 8 = 11 ...
     *
     * @return soma
     */
    public int somaTodosDigitos(String valor) {
        int soma = 0;

        for (int i = 0; i < valor.length(); i++) {
            soma += Integer.parseInt(String.valueOf(valor.charAt(i)));
        }

        return soma;
    }

    /**
     * funcao que calcula o valor de uma letra segundo as regras numerologicas
     * para as comparações utiliza-se o valor ascii
     */
    public int letraValue(char letra) {
        int valor = 0;
        switch (letra) {
            case 'A':
            case 'a':
            case 'J':
            case 'j':
            case 'S':
            case 's':
                valor = 1;
                break;

            case 'B':
            case 'b':
            case 'K':
            case 'k':
            case 'T':
            case 't':
                valor = 2;
                break;

            case 'C':
            case 'c':
            case 'L':
            case 'l':
            case 'U':
            case 'u':
                valor = 3;
                break;

            case 'D':
            case 'd':
            case 'M':
            case 'm':
            case 'V':
            case 'v':
                valor = 4;
                break;

            case 'E':
            case 'e':
            case 'N':
            case 'n':
            case 'W':
            case 'w':
                valor = 5;
                break;

            case 'F':
            case 'f':
            case 'O':
            case 'o':
            case 'X':
            case 'x':
                valor = 6;
                break;

            case 'G':
            case 'g':
            case 'P':
            case 'p':
            case 'Y':
            case 'y':
                valor = 7;
                break;

            case 'H':
            case 'h':
            case 'Q':
            case 'q':
            case 'Z':
            case 'z':
                valor = 8;
                break;

            case 'I':
            case 'i':
            case 'R':
            case 'r':
                valor = 9;
                break;
        }

        return valor;
    }

    /**
     * Number ano , Number sexo
     *
     * Realiza o calculo do kua utilizando a funcão somaTotal com o ano de nascimento da pessoa
     *
     * @retrun kua
     */
    public int somaKua(int ano ,int sexo) {
        int soma = somaTotal( ano );
        int kua;
        String str;

        //se for homem
        if ( sexo == 1 ) {
            kua = 11 - soma;

            //se for mulher
        } else {
            kua = 4 + soma;
        }
        str = kua + "";


        while ( str.length() > 1 ) {
            kua = somaTotal( kua );
            str = kua + "";
        }

        if ( sexo == 1 && kua == 5 ) {
            kua = 2;
        }

        if ( sexo == 2 && kua == 5 ) {
            kua = 8;
        }

        return kua;
    }
}
