package org.todeschini.service;

import org.todeschini.util.DateUtils;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

@ApplicationScoped
public class SignoService {

    @Inject
    DateUtils dateUtils;

    /**
     * dn Retorna o valor correspondente do signo da pessoa conforme a sua data de nascimento
     *
     * @return Number
     */
    public int getSigno(LocalDate aDate) {

        int value = 0;

        int ano = aDate.getYear();

        // variaveis d1 01/01
        LocalDate ld1 = LocalDate.of(ano, 1, 1);

        // variaveis d2 20/01
        LocalDate ld2 = LocalDate.of(ano, 1, 20);

        // variaveis d3 22/12
        LocalDate ld3 = LocalDate.of(ano, 12, 22);

        // variaveis d4 31/12
        LocalDate ld4 = LocalDate.of(ano, 12, 31);

        long dn = dateUtils.inMilliSeconds(aDate);
        long d1 = dateUtils.inMilliSeconds(ld1);
        long d2 = dateUtils.inMilliSeconds(ld2);
        long d3 = dateUtils.inMilliSeconds(ld3);
        long d4 = dateUtils.inMilliSeconds(ld4);


        if (((dn >= d1)) && (dn <= d2) || ((dn >= d3) && (dn <= d4))) {
            value = 1;

        } else {

            // variaveis d5 21/01
            LocalDate ld5 = LocalDate.of(ano, 1, 21);

            // variaveis d6 02/19
            LocalDate ld6 = LocalDate.of(ano, 2, 19);


            long d5 = dateUtils.inMilliSeconds(ld5);
            long d6 = dateUtils.inMilliSeconds(ld6);

            if ((dn >= d5) && (dn <= d6)) {
                value = 2;

            } else {

                // variaveis d7 20/02
                LocalDate ld7 = LocalDate.of(ano, 2, 20);

                // variaveis d8 20/03
                LocalDate ld8 = LocalDate.of(ano, 3, 20);

                long d7 = dateUtils.inMilliSeconds(ld7);
                long d8 = dateUtils.inMilliSeconds(ld8);

                if ((dn >= d7) && (dn <= d8)) {
                    value = 3;

                } else {

                    // variaveis d9 21/03
                    LocalDate ld9 = LocalDate.of(ano, 3, 21);

                    // variaveis d10 20/04
                    LocalDate ld10 = LocalDate.of(ano, 4, 20);

                    long d9 = dateUtils.inMilliSeconds(ld9);
                    long d10 = dateUtils.inMilliSeconds(ld10);

                    if ((dn >= d9) && (dn <= d10)) {
                        value = 4;

                    } else {

                        // variaveis d11 21/04
                        LocalDate ld11 = LocalDate.of(ano, 3, 21);

                        // variaveis d12 20/05
                        LocalDate ld12 = LocalDate.of(ano, 5, 20);

                        long d11 = dateUtils.inMilliSeconds(ld11);
                        long d12 = dateUtils.inMilliSeconds(ld12);

                        if ((dn >= d11) && (dn <= d12)) {
                            value = 5;

                        } else {

                            // variaveis d13 21/05
                            LocalDate ld13 = LocalDate.of(ano, 5, 21);

                            // variaveis d14 20/06

                            LocalDate ld14 = LocalDate.of(ano, 6, 20);

                            long d13 = dateUtils.inMilliSeconds(ld13);
                            long d14 = dateUtils.inMilliSeconds(ld14);

                            if ((dn >= d13) && (dn <= d14)) {
                                value = 6;

                            } else {

                                // variaveis d15 21/6
                                LocalDate ld15 = LocalDate.of(ano, 6, 21);

                                // variaveis d16 22/7
                                LocalDate ld16 = LocalDate.of(ano, 7, 22);

                                long d15 = dateUtils.inMilliSeconds(ld15);
                                long d16 = dateUtils.inMilliSeconds(ld16);

                                if ((dn >= d15) && (dn <= d16)) {
                                    value = 7;

                                } else {

                                    // variaveis d17 23/7
                                    LocalDate ld17 = LocalDate.of(ano, 7, 23);

                                    // variaveis d18 22/8
                                    LocalDate ld18 = LocalDate.of(ano, 8, 22);

                                    long d17 = dateUtils.inMilliSeconds(ld17);
                                    long d18 = dateUtils.inMilliSeconds(ld18);

                                    if ((dn >= d17) && (dn <= d18)) {
                                        value = 8;

                                    } else {

                                        //variaveis d19 23/8
                                        LocalDate ld19 = LocalDate.of(ano, 8, 23);

                                        // variaveis d20 22/9
                                        LocalDate ld20 = LocalDate.of(ano, 9, 22);

                                        long d19 = dateUtils.inMilliSeconds(ld19);
                                        long d20 = dateUtils.inMilliSeconds(ld20);

                                        if ((dn >= d19) && (dn <= d20)) {
                                            value = 9;

                                        } else {

                                            // variaveis d21 23/9
                                            LocalDate ld21 = LocalDate.of(ano, 9, 23);

                                            // variaveis d22 22/10
                                            LocalDate ld22 = LocalDate.of(ano, 10, 22);

                                            long d21 = dateUtils.inMilliSeconds(ld21);
                                            long d22 = dateUtils.inMilliSeconds(ld22);

                                            if ((dn >= d21) && (dn <= d22)) {
                                                value = 10;

                                            } else {

                                                // variaveis d23 23/10
                                                LocalDate ld23 = LocalDate.of(ano, 10, 23);
                                                // variaveis d24 21/11
                                                LocalDate ld24 = LocalDate.of(ano, 11, 21);

                                                long d23 = dateUtils.inMilliSeconds(ld23);
                                                long d24 = dateUtils.inMilliSeconds(ld24);

                                                if ((dn >= d23) && (dn <= d24)) {
                                                    value = 11;

                                                } else {

                                                     // variaveis d25 22/11
                                                    LocalDate ld25 = LocalDate.of(ano, 11, 22);

                                                     // variaveis d26 21/12
                                                    LocalDate ld26 = LocalDate.of(ano, 12, 21);

                                                    long d25 = dateUtils.inMilliSeconds(ld25);
                                                    long d26 = dateUtils.inMilliSeconds(ld26);

                                                    if ( ( dn  >= d25 ) && ( dn <= d26 ) ) {
                                                        value = 12;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return value;
    }
}
