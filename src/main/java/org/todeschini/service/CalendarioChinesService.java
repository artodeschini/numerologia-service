package org.todeschini.service;

import io.quarkus.runtime.StartupEvent;
import org.todeschini.util.DateUtils;
import org.todeschini.vo.CalculoVO;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class CalendarioChinesService {

    @Inject
    DateUtils dateUtils;

    private Map<Integer, CalculoVO.CalendarioChinesVO> calendario;

    public CalendarioChinesService() {
        onLoad();
    }

    public void onStart(@Observes StartupEvent ev) {
        onLoad();
    }

    public CalculoVO.CalendarioChinesVO getCalendarioChines(int ano) {
        return calendario.get(ano);
    }

    public int checkCalendarioChines(LocalDate date) {
        CalculoVO.CalendarioChinesVO chines;

        if ( date.getMonthValue() == 1 || date.getMonthValue() == 2 ) {
            chines = this.getCalendarioChines( date.getYear() );

            LocalDate dataInicio = chines.getInicio();
            LocalDate dataFim = chines.getFim();

            long dn = dateUtils.inMilliSeconds(date);
            long di = dateUtils.inMilliSeconds(dataInicio);
            long df = dateUtils.inMilliSeconds(dataFim);


            if ( df >= di && dn <= df ) {
                return date.getYear() - 1;

            } else {
                return date.getYear();
            }
        }
        return date.getYear();
    }

    private void onLoad() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy"); //"31-01-1900"

        calendario = new HashMap<>();

        calendario.put(1901, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-1900", formatter)).fim(LocalDate.parse("18-02-1901", formatter)).animal("Rato (Shu)").elemento("Metal").sinal(0).build());
        calendario.put(1902, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("19-02-1901", formatter)).fim(LocalDate.parse("07-02-1902", formatter)).animal("Boi/Búfalo (Niu)").elemento("Metal").sinal(1).build());
        calendario.put(1903, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-1902", formatter)).fim(LocalDate.parse("28-01-1903", formatter)).animal("Tigre (Hu)").elemento("Água").sinal(0).build());
        calendario.put(1904, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-1903", formatter)).fim(LocalDate.parse("15-02-1904", formatter)).animal("Coelho (Tu)").elemento("Água").sinal(1).build());
        calendario.put(1905, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("16-02-1904", formatter)).fim(LocalDate.parse("03-02-1905", formatter)).animal("Dragão (Long)").elemento("Madeira").sinal(0).build());
        calendario.put(1906, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("04-02-1905", formatter)).fim(LocalDate.parse("24-01-1906", formatter)).animal("Serpente (She)").elemento("Madeira").sinal(1).build());
        calendario.put(1907, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-01-1906", formatter)).fim(LocalDate.parse("12-02-1907", formatter)).animal("Cavalo (Ma)").elemento("Fogo").sinal(0).build());
        calendario.put(1908, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1907", formatter)).fim(LocalDate.parse("01-02-1908", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Fogo").sinal(1).build());
        calendario.put(1909, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-1908", formatter)).fim(LocalDate.parse("21-01-1909", formatter)).animal("Macaco (Hou)").elemento("Terra").sinal(0).build());
        calendario.put(1910, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("22-01-1909", formatter)).fim(LocalDate.parse("09-02-1910", formatter)).animal("Galo (Ji)").elemento("Terra").sinal(1).build());
        calendario.put(1911, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-1910", formatter)).fim(LocalDate.parse("29-01-1911", formatter)).animal("Cão (Gou)").elemento("Metal").sinal(0).build());
        calendario.put(1912, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("30-01-1911", formatter)).fim(LocalDate.parse("17-02-1912", formatter)).animal("Porco/Javali (Zhu)").elemento("Metal").sinal(1).build());
        calendario.put(1913, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("18-02-1912", formatter)).fim(LocalDate.parse("05-02-1913", formatter)).animal("Rato (Shu)").elemento("Água").sinal(0).build());
        calendario.put(1914, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-1913", formatter)).fim(LocalDate.parse("25-01-1914", formatter)).animal("Boi/Búfalo (Niu)").elemento("Água").sinal(1).build());
        calendario.put(1915, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("26-01-1914", formatter)).fim(LocalDate.parse("13-02-1915", formatter)).animal("Tigre (Hu)").elemento("Madeira").sinal(0).build());
        calendario.put(1916, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("14-02-1915", formatter)).fim(LocalDate.parse("12-02-1916", formatter)).animal("Coelho (Tu)").elemento("Madeira").sinal(1).build());
        calendario.put(1917, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1916", formatter)).fim(LocalDate.parse("22-01-1917", formatter)).animal("Dragão (Long)").elemento("Fogo").sinal(0).build());
        calendario.put(1918, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-1917", formatter)).fim(LocalDate.parse("10-02-1918", formatter)).animal("Serpente (She)").elemento("Fogo").sinal(1).build());
        calendario.put(1919, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("11-02-1918", formatter)).fim(LocalDate.parse("31-01-1919", formatter)).animal("Cavalo (Ma)").elemento("Terra").sinal(0).build());
        calendario.put(1920, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("01-02-1919", formatter)).fim(LocalDate.parse("19-02-1920", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Terra").sinal(1).build());
        calendario.put(1921, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("20-02-1920", formatter)).fim(LocalDate.parse("07-02-1921", formatter)).animal("Macaco (Hou)").elemento("Metal").sinal(0).build());
        calendario.put(1922, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-1921", formatter)).fim(LocalDate.parse("27-01-1922", formatter)).animal("Galo (Ji)").elemento("Metal").sinal(1).build());
        calendario.put(1923, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("28-01-1922", formatter)).fim(LocalDate.parse("15-02-1923", formatter)).animal("Cão (Gou)").elemento("Água").sinal(0).build());
        calendario.put(1924, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("16-02-1923", formatter)).fim(LocalDate.parse("04-02-1924", formatter)).animal("Porco/Javali (Zhu)").elemento("Água").sinal(1).build());
        calendario.put(1925, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("05-02-1924", formatter)).fim(LocalDate.parse("24-01-1925", formatter)).animal("Rato (Shu)").elemento("Madeira").sinal(0).build());
        calendario.put(1926, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-01-1925", formatter)).fim(LocalDate.parse("12-02-1926", formatter)).animal("Boi/Búfalo (Niu)").elemento("Madeira").sinal(1).build());
        calendario.put(1927, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1926", formatter)).fim(LocalDate.parse("01-02-1927", formatter)).animal("Tigre (Hu)").elemento("Fogo").sinal(0).build());
        calendario.put(1928, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-1927", formatter)).fim(LocalDate.parse("22-01-1928", formatter)).animal("Coelho (Tu)").elemento("Fogo").sinal(1).build());
        calendario.put(1929, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-1928", formatter)).fim(LocalDate.parse("09-02-1929", formatter)).animal("Dragão (Long)").elemento("Terra").sinal(0).build());
        calendario.put(1930, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-1929", formatter)).fim(LocalDate.parse("29-01-1930", formatter)).animal("Serpente (She)").elemento("Terra").sinal(1).build());
        calendario.put(1931, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("30-01-1930", formatter)).fim(LocalDate.parse("16-02-1931", formatter)).animal("Cavalo (Ma)").elemento("Metal").sinal(0).build());
        calendario.put(1932, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-1931", formatter)).fim(LocalDate.parse("05-02-1932", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Metal").sinal(1).build());
        calendario.put(1933, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-1932", formatter)).fim(LocalDate.parse("25-01-1933", formatter)).animal("Macaco (Hou)").elemento("Água").sinal(0).build());
        calendario.put(1934, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("26-01-1933", formatter)).fim(LocalDate.parse("13-02-1934", formatter)).animal("Galo (Ji)").elemento("Água").sinal(1).build());
        calendario.put(1935, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("14-02-1934", formatter)).fim(LocalDate.parse("03-02-1935", formatter)).animal("Cão (Gou)").elemento("Madeira").sinal(0).build());
        calendario.put(1936, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("04-02-1935", formatter)).fim(LocalDate.parse("23-01-1936", formatter)).animal("Porco/Javali (Zhu)").elemento("Madeira").sinal(1).build());
        calendario.put(1937, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("24-01-1936", formatter)).fim(LocalDate.parse("10-02-1937", formatter)).animal("Rato (Shu)").elemento("Fogo").sinal(0).build());
        calendario.put(1938, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("11-02-1937", formatter)).fim(LocalDate.parse("30-01-1938", formatter)).animal("Boi/Búfalo (Niu)").elemento("Fogo").sinal(1).build());
        calendario.put(1939, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-1938", formatter)).fim(LocalDate.parse("18-02-1939", formatter)).animal("Tigre (Hu)").elemento("Terra").sinal(0).build());
        calendario.put(1940, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("19-02-1939", formatter)).fim(LocalDate.parse("07-02-1940", formatter)).animal("Coelho (Tu)").elemento("Terra").sinal(1).build());
        calendario.put(1941, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-1940", formatter)).fim(LocalDate.parse("26-01-1941", formatter)).animal("Dragão (Long)").elemento("Metal").sinal(0).build());
        calendario.put(1942, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("27-01-1941", formatter)).fim(LocalDate.parse("14-02-1942", formatter)).animal("Serpente (She)").elemento("Metal").sinal(1).build());
        calendario.put(1943, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("15-02-1942", formatter)).fim(LocalDate.parse("04-02-1943", formatter)).animal("Cavalo (Ma)").elemento("Água").sinal(0).build());
        calendario.put(1944, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("05-02-1943", formatter)).fim(LocalDate.parse("24-01-1944", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Água").sinal(1).build());
        calendario.put(1945, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-01-1944", formatter)).fim(LocalDate.parse("12-02-1945", formatter)).animal("Macaco (Hou)").elemento("Madeira").sinal(0).build());
        calendario.put(1946, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1945", formatter)).fim(LocalDate.parse("01-02-1946", formatter)).animal("Galo (Ji)").elemento("Madeira").sinal(1).build());
        calendario.put(1947, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-1946", formatter)).fim(LocalDate.parse("21-01-1947", formatter)).animal("Cão (Gou)").elemento("Fogo").sinal(0).build());
        calendario.put(1948, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("22-01-1947", formatter)).fim(LocalDate.parse("09-02-1948", formatter)).animal("Porco/Javali (Zhu)").elemento("Fogo").sinal(1).build());
        calendario.put(1949, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-1948", formatter)).fim(LocalDate.parse("28-01-1949", formatter)).animal("Rato (Shu)").elemento("Terra").sinal(0).build());
        calendario.put(1950, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-1949", formatter)).fim(LocalDate.parse("16-02-1950", formatter)).animal("Boi/Búfalo (Niu)").elemento("Terra").sinal(1).build());
        calendario.put(1951, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-1950", formatter)).fim(LocalDate.parse("05-02-1951", formatter)).animal("Tigre (Hu)").elemento("Metal").sinal(0).build());
        calendario.put(1952, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-1951", formatter)).fim(LocalDate.parse("26-01-1952", formatter)).animal("Coelho (Tu)").elemento("Metal").sinal(1).build());
        calendario.put(1953, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("27-01-1952", formatter)).fim(LocalDate.parse("13-02-1953", formatter)).animal("Dragão (Long)").elemento("Água").sinal(0).build());
        calendario.put(1954, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("14-02-1953", formatter)).fim(LocalDate.parse("02-02-1954", formatter)).animal("Serpente (She)").elemento("Água").sinal(1).build());
        calendario.put(1955, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("03-02-1954", formatter)).fim(LocalDate.parse("23-01-1955", formatter)).animal("Cavalo (Ma)").elemento("Madeira").sinal(0).build());
        calendario.put(1956, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("24-01-1955", formatter)).fim(LocalDate.parse("11-02-1956", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Madeira").sinal(1).build());
        calendario.put(1957, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("12-02-1956", formatter)).fim(LocalDate.parse("30-01-1957", formatter)).animal("Macaco (Hou)").elemento("Fogo").sinal(0).build());
        calendario.put(1958, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-1957", formatter)).fim(LocalDate.parse("17-02-1958", formatter)).animal("Galo (Ji)").elemento("Fogo").sinal(1).build());
        calendario.put(1959, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("18-02-1958", formatter)).fim(LocalDate.parse("07-02-1959", formatter)).animal("Cão (Gou)").elemento("Terra").sinal(0).build());
        calendario.put(1960, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-1959", formatter)).fim(LocalDate.parse("27-01-1960", formatter)).animal("Porco/Javali (Zhu)").elemento("Terra").sinal(1).build());
        calendario.put(1961, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("28-01-1960", formatter)).fim(LocalDate.parse("14-01-1961", formatter)).animal("Rato (Shu)").elemento("Metal").sinal(0).build());
        calendario.put(1962, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("15-01-1961", formatter)).fim(LocalDate.parse("04-02-1962", formatter)).animal("Boi/Búfalo (Niu)").elemento("Metal").sinal(1).build());
        calendario.put(1963, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("05-02-1962", formatter)).fim(LocalDate.parse("24-01-1963", formatter)).animal("Tigre (Hu)").elemento("Água").sinal(0).build());
        calendario.put(1964, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-01-1963", formatter)).fim(LocalDate.parse("12-02-1964", formatter)).animal("Coelho (Tu)").elemento("Água").sinal(1).build());
        calendario.put(1965, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1964", formatter)).fim(LocalDate.parse("01-02-1965", formatter)).animal("Dragão (Long)").elemento("Madeira").sinal(0).build());
        calendario.put(1966, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-1965", formatter)).fim(LocalDate.parse("20-01-1966", formatter)).animal("Serpente (She)").elemento("Madeira").sinal(1).build());
        calendario.put(1967, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("21-01-1966", formatter)).fim(LocalDate.parse("08-02-1967", formatter)).animal("Cavalo (Ma)").elemento("Fogo").sinal(0).build());
        calendario.put(1968, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("09-02-1967", formatter)).fim(LocalDate.parse("29-01-1968", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Fogo").sinal(1).build());
        calendario.put(1969, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("30-01-1968", formatter)).fim(LocalDate.parse("16-02-1969", formatter)).animal("Macaco (Hou)").elemento("Terra").sinal(0).build());
        calendario.put(1970, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-1969", formatter)).fim(LocalDate.parse("05-02-1970", formatter)).animal("Galo (Ji)").elemento("Terra").sinal(1).build());
        calendario.put(1971, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-1970", formatter)).fim(LocalDate.parse("26-01-1971", formatter)).animal("Cão (Gou)").elemento("Metal").sinal(0).build());
        calendario.put(1972, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("27-01-1971", formatter)).fim(LocalDate.parse("15-01-1972", formatter)).animal("Porco/Javali (Zhu)").elemento("Metal").sinal(1).build());
        calendario.put(1973, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("16-01-1972", formatter)).fim(LocalDate.parse("02-02-1973", formatter)).animal("Rato (Shu)").elemento("Água").sinal(0).build());
        calendario.put(1974, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("03-02-1973", formatter)).fim(LocalDate.parse("22-01-1974", formatter)).animal("Boi/Búfalo (Niu)").elemento("Água").sinal(1).build());
        calendario.put(1975, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-1974", formatter)).fim(LocalDate.parse("10-02-1975", formatter)).animal("Tigre (Hu)").elemento("Madeira").sinal(0).build());
        calendario.put(1976, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("11-02-1975", formatter)).fim(LocalDate.parse("30-01-1976", formatter)).animal("Coelho (Tu)").elemento("Madeira").sinal(1).build());
        calendario.put(1977, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-1976", formatter)).fim(LocalDate.parse("17-02-1977", formatter)).animal("Dragão (Long)").elemento("Fogo").sinal(0).build());
        calendario.put(1978, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("18-02-1977", formatter)).fim(LocalDate.parse("06-02-1978", formatter)).animal("Serpente (She)").elemento("Fogo").sinal(1).build());
        calendario.put(1979, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("07-02-1978", formatter)).fim(LocalDate.parse("27-01-1979", formatter)).animal("Cavalo (Ma)").elemento("Terra").sinal(0).build());
        calendario.put(1980, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("28-01-1979", formatter)).fim(LocalDate.parse("15-02-1980", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Terra").sinal(1).build());
        calendario.put(1981, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("16-02-1980", formatter)).fim(LocalDate.parse("04-02-1981", formatter)).animal("Macaco (Hou)").elemento("Metal").sinal(0).build());
        calendario.put(1982, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("05-02-1981", formatter)).fim(LocalDate.parse("24-02-1982", formatter)).animal("Galo (Ji)").elemento("Metal").sinal(1).build());
        calendario.put(1983, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-02-1982", formatter)).fim(LocalDate.parse("12-02-1983", formatter)).animal("Cão (Gou)").elemento("Água").sinal(0).build());
        calendario.put(1984, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-1983", formatter)).fim(LocalDate.parse("01-02-1984", formatter)).animal("Porco/Javali (Zhu)").elemento("Água").sinal(1).build());
        calendario.put(1985, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-1984", formatter)).fim(LocalDate.parse("19-02-1985", formatter)).animal("Rato (Shu)").elemento("Madeira").sinal(0).build());
        calendario.put(1986, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("20-02-1985", formatter)).fim(LocalDate.parse("08-02-1986", formatter)).animal("Boi/Búfalo (Niu)").elemento("Madeira").sinal(1).build());
        calendario.put(1987, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("09-02-1986", formatter)).fim(LocalDate.parse("28-01-1987", formatter)).animal("Tigre (Hu)").elemento("Fogo").sinal(0).build());
        calendario.put(1988, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-1987", formatter)).fim(LocalDate.parse("16-02-1988", formatter)).animal("Coelho (Tu)").elemento("Fogo").sinal(1).build());
        calendario.put(1989, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-1988", formatter)).fim(LocalDate.parse("05-02-1989", formatter)).animal("Dragão (Long)").elemento("Terra").sinal(0).build());
        calendario.put(1990, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-1989", formatter)).fim(LocalDate.parse("26-01-1990", formatter)).animal("Serpente (She)").elemento("Terra").sinal(1).build());
        calendario.put(1991, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("27-01-1990", formatter)).fim(LocalDate.parse("14-02-1991", formatter)).animal("Cavalo (Ma)").elemento("Metal").sinal(0).build());
        calendario.put(1992, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("15-02-1991", formatter)).fim(LocalDate.parse("03-02-1992", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Metal").sinal(1).build());
        calendario.put(1993, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("04-02-1992", formatter)).fim(LocalDate.parse("22-01-1993", formatter)).animal("Macaco (Hou)").elemento("Água").sinal(0).build());
        calendario.put(1994, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-1993", formatter)).fim(LocalDate.parse("09-02-1994", formatter)).animal("Galo (Ji)").elemento("Água").sinal(1).build());
        calendario.put(1995, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-1994", formatter)).fim(LocalDate.parse("30-01-1995", formatter)).animal("Cão (Gou)").elemento("Madeira").sinal(0).build());
        calendario.put(1996, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-1995", formatter)).fim(LocalDate.parse("18-02-1996", formatter)).animal("Porco/Javali (Zhu)").elemento("Madeira").sinal(1).build());
        calendario.put(1997, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("19-02-1996", formatter)).fim(LocalDate.parse("06-02-1997", formatter)).animal("Rato (Shu)").elemento("Fogo").sinal(0).build());
        calendario.put(1998, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("07-02-1997", formatter)).fim(LocalDate.parse("28-01-1998", formatter)).animal("Boi/Búfalo (Niu)").elemento("Fogo").sinal(1).build());
        calendario.put(1999, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-1998", formatter)).fim(LocalDate.parse("15-02-1999", formatter)).animal("Tigre (Hu)").elemento("Terra").sinal(0).build());
        calendario.put(2000, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("16-02-1999", formatter)).fim(LocalDate.parse("05-02-2000", formatter)).animal("Coelho (Tu)").elemento("Terra").sinal(1).build());
        calendario.put(2001, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-2000", formatter)).fim(LocalDate.parse("24-01-2001", formatter)).animal("Dragão (Long)").elemento("Metal").sinal(0).build());
        calendario.put(2002, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("25-01-2001", formatter)).fim(LocalDate.parse("11-02-2002", formatter)).animal("Serpente (She)").elemento("Metal").sinal(1).build());
        calendario.put(2003, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("12-02-2002", formatter)).fim(LocalDate.parse("31-01-2003", formatter)).animal("Cavalo (Ma)").elemento("Água").sinal(0).build());
        calendario.put(2004, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("01-02-2003", formatter)).fim(LocalDate.parse("20-01-2004", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Água").sinal(1).build());
        calendario.put(2005, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("21-01-2004", formatter)).fim(LocalDate.parse("07-02-2005", formatter)).animal("Macaco (Hou)").elemento("Madeira").sinal(0).build());
        calendario.put(2006, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-2005", formatter)).fim(LocalDate.parse("28-01-2006", formatter)).animal("Galo (Ji)").elemento("Madeira").sinal(1).build());
        calendario.put(2007, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-2006", formatter)).fim(LocalDate.parse("16-02-2007", formatter)).animal("Cão (Gou)").elemento("Fogo").sinal(0).build());
        calendario.put(2008, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-2007", formatter)).fim(LocalDate.parse("06-02-2008", formatter)).animal("Porco/Javali (Zhu)").elemento("Fogo").sinal(1).build());
        calendario.put(2009, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("07-02-2008", formatter)).fim(LocalDate.parse("25-01-2009", formatter)).animal("Rato (Shu)").elemento("Terra").sinal(0).build());
        calendario.put(2010, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("26-01-2009", formatter)).fim(LocalDate.parse("13-02-2010", formatter)).animal("Boi/Búfalo (Niu)").elemento("Terra").sinal(1).build());
        calendario.put(2011, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("14-02-2010", formatter)).fim(LocalDate.parse("02-02-2011", formatter)).animal("Tigre (Hu)").elemento("Metal").sinal(0).build());
        calendario.put(2012, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("03-02-2011", formatter)).fim(LocalDate.parse("22-01-2012", formatter)).animal("Coelho (Tu)").elemento("Metal").sinal(1).build());
        calendario.put(2013, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-2012", formatter)).fim(LocalDate.parse("09-02-2013", formatter)).animal("Dragão (Long)").elemento("Água").sinal(0).build());
        calendario.put(2014, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-2013", formatter)).fim(LocalDate.parse("20-01-2014", formatter)).animal("Serpente (She)").elemento("Água").sinal(1).build());
        calendario.put(2015, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-2014", formatter)).fim(LocalDate.parse("18-02-2015", formatter)).animal("Cavalo (Ma)").elemento("Madeira").sinal(0).build());
        calendario.put(2016, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("19-02-2015", formatter)).fim(LocalDate.parse("07-01-2016", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Madeira").sinal(1).build());
        calendario.put(2017, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-01-2016", formatter)).fim(LocalDate.parse("27-01-2017", formatter)).animal("Macaco (Hou)").elemento("Fogo").sinal(0).build());
        calendario.put(2018, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("28-01-2017", formatter)).fim(LocalDate.parse("14-02-2018", formatter)).animal("Galo (Ji)").elemento("Fogo").sinal(1).build());
        calendario.put(2019, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("15-02-2018", formatter)).fim(LocalDate.parse("03-01-2019", formatter)).animal("Cão (Gou)").elemento("Terra").sinal(0).build());
        calendario.put(2020, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("04-01-2019", formatter)).fim(LocalDate.parse("23-01-2020", formatter)).animal("Porco/Javali (Zhu)").elemento("Terra").sinal(1).build());
        calendario.put(2021, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("24-01-2020", formatter)).fim(LocalDate.parse("10-02-2021", formatter)).animal("Rato (Shu)").elemento("Metal").sinal(0).build());
        calendario.put(2022, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("11-02-2021", formatter)).fim(LocalDate.parse("31-01-2022", formatter)).animal("Boi/Búfalo (Niu)").elemento("Metal").sinal(1).build());
        calendario.put(2023, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("01-02-2022", formatter)).fim(LocalDate.parse("20-01-2023", formatter)).animal("Tigre (Hu)").elemento("Água").sinal(0).build());
        calendario.put(2024, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("21-01-2023", formatter)).fim(LocalDate.parse("08-02-2024", formatter)).animal("Coelho (Tu)").elemento("Água").sinal(1).build());
        calendario.put(2025, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("09-02-2024", formatter)).fim(LocalDate.parse("28-01-2025", formatter)).animal("Dragão (Long)").elemento("Madeira").sinal(0).build());
        calendario.put(2026, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("29-01-2025", formatter)).fim(LocalDate.parse("16-02-2026", formatter)).animal("Serpente (She)").elemento("Madeira").sinal(1).build());
        calendario.put(2027, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-2026", formatter)).fim(LocalDate.parse("05-02-2027", formatter)).animal("Cavalo (Ma)").elemento("Fogo").sinal(0).build());
        calendario.put(2028, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-2027", formatter)).fim(LocalDate.parse("25-01-2028", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Fogo").sinal(1).build());
        calendario.put(2029, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("26-01-2028", formatter)).fim(LocalDate.parse("12-02-2029", formatter)).animal("Macaco (Hou)").elemento("Terra").sinal(0).build());
        calendario.put(2030, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("13-02-2029", formatter)).fim(LocalDate.parse("02-02-2030", formatter)).animal("Galo (Ji)").elemento("Terra").sinal(1).build());
        calendario.put(2031, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("03-02-2030", formatter)).fim(LocalDate.parse("22-01-2031", formatter)).animal("Cão (Gou)").elemento("Metal").sinal(0).build());
        calendario.put(2032, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("23-01-2031", formatter)).fim(LocalDate.parse("10-02-2032", formatter)).animal("Porco/Javali (Zhu)").elemento("Metal").sinal(1).build());
        calendario.put(2033, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("11-02-2032", formatter)).fim(LocalDate.parse("30-01-2033", formatter)).animal("Rato (Shu)").elemento("Água").sinal(0).build());
        calendario.put(2034, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("31-01-2033", formatter)).fim(LocalDate.parse("18-02-2034", formatter)).animal("Boi/Búfalo (Niu)").elemento("Água").sinal(1).build());
        calendario.put(2035, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("19-02-2034", formatter)).fim(LocalDate.parse("07-02-2035", formatter)).animal("Tigre (Hu)").elemento("Madeira").sinal(0).build());
        calendario.put(2036, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("08-02-2035", formatter)).fim(LocalDate.parse("27-01-2036", formatter)).animal("Coelho (Tu)").elemento("Madeira").sinal(1).build());
        calendario.put(2037, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("28-01-2036", formatter)).fim(LocalDate.parse("14-02-2037", formatter)).animal("Dragão (Long)").elemento("Fogo").sinal(0).build());
        calendario.put(2038, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("15-02-2037", formatter)).fim(LocalDate.parse("03-02-2038", formatter)).animal("Serpente (She)").elemento("Fogo").sinal(1).build());
        calendario.put(2039, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("04-02-2038", formatter)).fim(LocalDate.parse("23-01-2039", formatter)).animal("Cavalo (Ma)").elemento("Terra").sinal(0).build());
        calendario.put(2040, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("24-01-2039", formatter)).fim(LocalDate.parse("11-02-2040", formatter)).animal("Carneiro/Cabra (Yang)").elemento("Terra").sinal(1).build());
        calendario.put(2041, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("12-02-2040", formatter)).fim(LocalDate.parse("31-01-2041", formatter)).animal("Macaco (Hou)").elemento("Metal").sinal(0).build());
        calendario.put(2042, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("01-02-2041", formatter)).fim(LocalDate.parse("21-01-2042", formatter)).animal("Galo (Ji)").elemento("Metal").sinal(1).build());
        calendario.put(2043, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("22-01-2042", formatter)).fim(LocalDate.parse("09-02-2043", formatter)).animal("Cão (Gou)").elemento("Água").sinal(0).build());
        calendario.put(2044, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("10-02-2043", formatter)).fim(LocalDate.parse("29-01-2044", formatter)).animal("Porco/Javali (Zhu)").elemento("Água").sinal(1).build());
        calendario.put(2045, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("30-01-2044", formatter)).fim(LocalDate.parse("16-02-2045", formatter)).animal("Rato (Shu)").elemento("Madeira").sinal(0).build());
        calendario.put(2046, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("17-02-2045", formatter)).fim(LocalDate.parse("05-02-2046", formatter)).animal("Boi/Búfalo (Niu)").elemento("Madeira").sinal(1).build());
        calendario.put(2047, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("06-02-2046", formatter)).fim(LocalDate.parse("25-01-2047", formatter)).animal("Tigre (Hu)").elemento("Fogo").sinal(0).build());
        calendario.put(2048, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("26-01-2047", formatter)).fim(LocalDate.parse("13-02-2048", formatter)).animal("Coelho (Tu)").elemento("Fogo").sinal(1).build());
        calendario.put(2049, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("14-02-2048", formatter)).fim(LocalDate.parse("01-02-2049", formatter)).animal("Dragão (Long)").elemento("Terra").sinal(0).build());
        calendario.put(2050, CalculoVO.CalendarioChinesVO.builder().inicio(LocalDate.parse("02-02-2049", formatter)).fim(LocalDate.parse("22-01-2050", formatter)).animal("Serpente (She)").elemento("Terra").sinal(1).build());
    }
}
